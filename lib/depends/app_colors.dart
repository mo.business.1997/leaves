import 'package:flutter/material.dart';

class AppColors {
  static const Color grey = Color(0xff8e8e8e);
  static const Color blackCustom = Color(0xfff2f2f2);
  static const Color platinum = Color(0xffc3c9cf);
  static Color error = Colors.red.shade600;
  static Color mainColor = const Color(0xFF083c53);
  static Color secondColor = const Color(0xFF14506b);
  static Color secColor = const Color(0xFF1E465A);
  static Color thirdColor = const Color(0xFF397796);
  static Color buttonColor = const Color(0xFFFFCC33);
  static Color whiteColor = Colors.white;
  static Color logoTextColor = const Color(0x80EEEEEE);
  static Color mainlightColor = const Color(0xFF5F94FF);
  static Color grayColor = const Color(0xFF454545);
  static Color meduimGrayColor = const Color(0xFFE6E6E6);
  static Color lightgrayColor = const Color(0xFFE1E3E4);
  static Color lightgray2Color = const Color.fromARGB(255, 237, 237, 237);
  static Color blackColor = Colors.black;
  static Color planNameColor = const Color(0xFFCED1D7);
  static Color textColor = const Color(0xFF023246); //كحلي
  static Color primaryTextColor = const Color(0xFF287094); //بترولي
  static Color productNameColor = const Color(0xFFC54D96);
  static Color blueColor = const Color(0xFF595AE6);
  static Color redColor = const Color(0xFFED254E);

  static Color kTextColor = const Color(0xff757575); //0xff3c4046
  static Color kBackgroundColor = const Color(0xFFF9F8FD);
  static LinearGradient mainGradientColor = const LinearGradient(
    colors: [
      Color(0xFF397796),
      Color(0xFF14506b),
      Color(0xFF083c53),
    ],
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
  );

  static LinearGradient reversedMainGradientColor = const LinearGradient(
    colors: [
      Color(0xFF083c53),
      Color(0xFF14506b),
      Color(0xFF397796),
    ],
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
  );

  static LinearGradient favouriteGradient = LinearGradient(
    colors: [
      AppColors.buttonColor,
      AppColors.redColor,
    ],
    begin: Alignment.centerRight,
    end: Alignment.centerLeft,
  );

  static LinearGradient profileGradient = LinearGradient(
    colors: [
      AppColors.textColor,
      AppColors.textColor.withOpacity(0.9),
      AppColors.textColor.withOpacity(0.8),
      AppColors.textColor.withOpacity(0.7),
      AppColors.textColor.withOpacity(0.6),
      AppColors.textColor.withOpacity(0.5),
      AppColors.textColor.withOpacity(0.4),
      AppColors.textColor.withOpacity(0.3),
      AppColors.textColor.withOpacity(0.2),
      AppColors.textColor.withOpacity(0.1),
      AppColors.textColor.withOpacity(0.0),
      Colors.transparent
    ],
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
  );

  static LinearGradient lightGradientColor = const LinearGradient(
    colors: [
      Color(0xFF14506b),
      Color(0xFF14506b),
      Color(0xFF397796),
      Color(0xFFFFFFFF),
    ],
    stops: [0.0, 0.2, 0.6, 0.9],
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
  );
  static MaterialColor primaryColor = MaterialColor(0xFF083c53, _color);
  static final Map<int, Color> _color = {
    50: const Color.fromRGBO(20, 80, 107, .1),
    100: const Color.fromRGBO(20, 80, 107, .2),
    200: const Color.fromRGBO(20, 80, 107, .3),
    300: const Color.fromRGBO(20, 80, 107, .4),
    400: const Color.fromRGBO(20, 80, 107, .5),
    500: const Color.fromRGBO(20, 80, 107, .6),
    600: const Color.fromRGBO(20, 80, 107, .7),
    700: const Color.fromRGBO(20, 80, 107, .8),
    800: const Color.fromRGBO(20, 80, 107, .9),
    900: const Color.fromRGBO(20, 80, 107, 1),
  };
  static LinearGradient statisticsGradientColor = LinearGradient(
    colors: [
      AppColors.primaryTextColor,
      AppColors.textColor,
    ],
    begin: Alignment.topRight,
    end: Alignment.bottomLeft,
  );
}
