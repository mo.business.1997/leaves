import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hnndes_project/depends/app_colors.dart';

class AppStyles {
  static Duration kAnimationDuration = const Duration(milliseconds: 200);
  static double kDefaultPadding = 20.0;

  static Opacity backgroundShapes = Opacity(
    // opacity: 0.25,
    opacity: 0.95,
    child: Container(
      height: double.infinity,
      width: double.infinity,
      decoration: const BoxDecoration(),
      child: Image.asset(
        "assets/images/background_shapes.png",
        fit: BoxFit.cover,
      ),
    ),
  );

  static Opacity coloredBackgroundShapes = Opacity(
    // opacity: 0.25,
    opacity: 0.05,
    child: Container(
      height: double.infinity,
      width: double.infinity,
      decoration: const BoxDecoration(),
      child: Image.asset(
        "assets/images/background_shapes_colored.png",
        fit: BoxFit.fill,
      ),
    ),
  );

  static InputDecoration cInputDecoration(String label, String hint) {
    return InputDecoration(
      label: Text(label.toString()),
      hintText: hint.toString(),
      floatingLabelBehavior: FloatingLabelBehavior.always,
      contentPadding: const EdgeInsets.symmetric(horizontal: 40, vertical: 10),
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(12),
        borderSide: BorderSide(color: AppColors.kTextColor),
        gapPadding: 10,
      ),
      focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(12),
        borderSide: BorderSide(color: AppColors.kTextColor),
        gapPadding: 10,
      ),
      errorBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(12),
        borderSide: const BorderSide(color: Colors.red),
        gapPadding: 10,
      ),
      focusedErrorBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(12),
        borderSide: const BorderSide(color: Colors.red),
        gapPadding: 10,
      ),
    );
  }

  static InputDecoration textInputDecoration(String label) {
    return InputDecoration(
      isDense: true,
      label: Text(label.toString()),
      // hintText: hint.toString(),
      // floatingLabelBehavior: FloatingLabelBehavior.always,
      contentPadding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 10.h),
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(12.r),
        borderSide: BorderSide(color: AppColors.mainColor),
        // gapPadding: 10,
      ),
      focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(12.r),
        borderSide: BorderSide(color: AppColors.mainColor),
        // gapPadding: 10,
      ),
      errorBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(12.r),
        borderSide: const BorderSide(color: Colors.red),
        // gapPadding: 10,
      ),
      focusedErrorBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(12.r),
        borderSide: const BorderSide(color: Colors.red),
        // gapPadding: 10,
      ),
    );
  }

  static InputDecoration reservationInputDecoration(String label) {
    return InputDecoration(
      isDense: true,
      filled: true,
      fillColor: Colors.white,
      label: Text(label),
      enabledBorder: OutlineInputBorder(
        borderSide: const BorderSide(color: Color(0xFFCED1D7)),
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(22.r),
          bottomLeft: Radius.circular(22.r),
        ),
      ),
      border: OutlineInputBorder(
        borderSide: const BorderSide(color: Color(0xFFCED1D7)),
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(22.r),
          bottomLeft: Radius.circular(22.r),
        ),
      ),
    );
  }

  static TextStyle primaryTextStyle() => TextStyle(
        fontSize: 20.sp,
        color: AppColors.blackColor,
      );

  static TextStyle secondaryTextStyle() => TextStyle(
        fontSize: 24.sp,
        color: AppColors.primaryTextColor,
      );
}
