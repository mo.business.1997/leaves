import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hnndes_project/depends/app_colors.dart';
import 'package:hnndes_project/depends/styles.dart';

class InputField extends StatelessWidget {
  final FormFieldValidator<String>? validator;
  final void Function(String?)? onsaved;
  final ValueChanged<String>? onFieldSubmitted;
  final String? text;
  final TextInputType? keyboardType;
  final String? label;
  final Widget? icon;
  final Widget? sufexicon;
  final int? maxLength;
  final Color? color;
  final Color? textColor;
  final List<TextInputFormatter>? inputFormatters;
  final bool? alignLabelWithHint;
  final int? maxLines;
  final int? minLines;
  final int? fontSize;
  final double? borderRadius;
  final TextEditingController? controller;
  final double? fieldWidth;
  final String? hint;
  final Color? hintColor;
  final TextStyle? counterStyle;
  final bool enabled;
  final void Function(String?)? onChanged;
  final EdgeInsets? contentPadding;
  final InputDecoration? decoration;
  final TextDirection? textDirection;
  final TextStyle? style;
  final Color? cursorColor;
  final TextInputAction? textInputAction;
  final double? borderwidth;

  const InputField(
      {Key? key,
      this.validator,
      this.onsaved,
      this.onFieldSubmitted,
      this.text,
      this.keyboardType,
      this.label,
      this.icon,
      this.maxLength,
      this.color,
      this.textColor,
      this.inputFormatters,
      this.alignLabelWithHint,
      this.maxLines,
      this.minLines,
      this.fontSize,
      this.borderRadius = 0,
      this.controller,
      this.fieldWidth,
      this.hint,
      this.hintColor,
      this.counterStyle,
      this.enabled = true,
      this.onChanged,
      this.contentPadding,
      this.decoration,
      this.textDirection,
      this.style,
      this.cursorColor,
      this.sufexicon,
      this.textInputAction,
      this.borderwidth})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: fieldWidth?.w ?? double.maxFinite,
      child: FocusScope(
        child: Focus(
          onFocusChange: (t) {
            t
                ? controller?.selection = TextSelection.fromPosition(
                    TextPosition(offset: controller?.text.length ?? 0))
                : null;
          },
          child: TextFormField(
            textDirection: TextDirection.ltr,
            textCapitalization: TextCapitalization.sentences,
            initialValue: text,
            enabled: enabled,
            minLines: maxLines ?? 1,
            style: style ??
                AppStyles.primaryTextStyle().copyWith(
                    fontSize: fontSize?.sp ?? 22.sp,
                    color: textColor ?? AppColors.mainColor),
            keyboardType: keyboardType,
            maxLength: maxLength,
            cursorColor: cursorColor ?? AppColors.mainColor,
            validator: validator,
            onSaved: onsaved,
            onFieldSubmitted: onFieldSubmitted,
            onChanged: onChanged,
            inputFormatters: inputFormatters,
            maxLines: maxLines ?? 1,
            controller: controller,
            textInputAction: textInputAction ?? TextInputAction.newline,
            decoration: decoration ??
                InputDecoration(
                  isDense: true,
                  suffixIcon: sufexicon,
                  hintText: hint,
                  hintStyle: AppStyles.primaryTextStyle().copyWith(
                    fontSize: 15.sp,
                    color: hintColor ?? AppColors.whiteColor.withOpacity(0.7),
                  ),
                  counterStyle:
                      counterStyle ?? TextStyle(color: AppColors.whiteColor),
                  contentPadding: contentPadding ??
                      EdgeInsets.symmetric(horizontal: 10.0.w, vertical: 15.h),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: AppColors.mainColor),
                    borderRadius: BorderRadius.circular(borderRadius!),
                  ),
                  border: OutlineInputBorder(
                    borderSide: BorderSide(color: AppColors.mainColor),
                    borderRadius: BorderRadius.circular(borderRadius!),
                  ),
                  errorBorder: OutlineInputBorder(
                    borderSide: BorderSide(width: 2.w, color: Colors.red),
                    borderRadius: BorderRadius.zero,
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        width: borderwidth ?? 4.w, color: AppColors.mainColor),
                    borderRadius: BorderRadius.circular(borderRadius!),
                  ),
                  floatingLabelBehavior: FloatingLabelBehavior.auto,
                  fillColor: color ?? AppColors.mainColor,
                  errorStyle: const TextStyle(
                      color: Colors.redAccent, fontWeight: FontWeight.bold),
                  filled: true,
                  labelText: label,
                  labelStyle: AppStyles.primaryTextStyle().copyWith(
                      color: Colors.grey[500], fontSize: fontSize?.sp ?? 20.sp),
                  prefixIcon: icon,
                  alignLabelWithHint: alignLabelWithHint,
                ),
          ),
        ),
      ),
    );
  }
}
