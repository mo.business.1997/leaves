class LeaveModel {
  int? id;
  String? employeeName;
  int? statusId;
  String? statusName;
  String? absenceValue;
  String? number;
  int? employeeId;
  int? typeId;
  String? absenceFrom;
  String? absenceTo;
  String? notes;

  LeaveModel(
      {this.id,
      this.employeeName,
      this.statusId,
      this.statusName,
      this.absenceValue,
      this.number,
      this.employeeId,
      this.typeId,
      this.absenceFrom,
      this.absenceTo,
      this.notes});

  LeaveModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    employeeName = json['employeeName'];
    statusId = json['statusId'];
    statusName = json['statusName'];
    absenceValue = json['absenceValue'];
    number = json['number'];
    employeeId = json['employeeId'];
    typeId = json['typeId'];
    absenceFrom = json['absenceFrom'];
    absenceTo = json['absenceTo'];
    notes = json['notes'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['employeeName'] = employeeName;
    data['statusId'] = statusId;
    data['statusName'] = statusName;
    data['absenceValue'] = absenceValue;
    data['number'] = number;
    data['employeeId'] = employeeId;
    data['typeId'] = typeId;
    data['absenceFrom'] = absenceFrom;
    data['absenceTo'] = absenceTo;
    data['notes'] = notes;
    return data;
  }
}
