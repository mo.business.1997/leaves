import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hnndes_project/api/signin_api.dart';
import 'package:hnndes_project/blocs/signin_bloc/signin_bloc.dart';
import 'package:hnndes_project/depends/app_colors.dart';
import 'package:hnndes_project/depends/styles.dart';
import 'package:hnndes_project/pages/leaves_page.dart';
import 'package:hnndes_project/widgets/input_field.dart';
import 'package:hnndes_project/widgets/loading_widget.dart';

class SigninPage extends StatelessWidget {
  SigninPage({super.key});
  final SigninBloc signinBloc = SigninBloc(signinApi: SigninApi());
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => signinBloc..add(InitEvent()),
      child: SafeArea(
        child: Scaffold(
          backgroundColor: AppColors.whiteColor,
          body: BlocConsumer<SigninBloc, SigninState>(
            listener: (context, state) {
              if (state is SigninSuccess) {
                ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                  content: Text("Signin success"),
                  backgroundColor: Colors.green,
                  duration: Duration(seconds: 2),
                ));
                Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (context) => LeavesPage(),
                ));
              } else if (state is SigninFailure) {
                ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                  content: const Text("Signin Failed"),
                  backgroundColor: AppColors.error,
                  duration: const Duration(seconds: 2),
                ));
              }
            },
            builder: (context, state) {
              if (state is LoadingState) {
                return const LoadingWidget();
              }
              return Padding(
                padding: EdgeInsets.symmetric(horizontal: 32.w),
                child: SingleChildScrollView(
                  child: GestureDetector(
                    behavior: HitTestBehavior.opaque,
                    onTap: () => FocusManager.instance.primaryFocus!.unfocus(),
                    child: SizedBox(
                      height: ScreenUtil().screenHeight,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Center(
                            child: SizedBox(
                              width: 366.w,
                              height: 212.h,
                              child: Image.asset("assets/images/Welcome.png"),
                            ),
                          ),
                          SizedBox(
                            height: 32.h,
                          ),
                          Center(
                            child: Text(
                              "Please Login To Your Account",
                              style: AppStyles.primaryTextStyle().copyWith(
                                fontSize: 28.sp,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 32.h,
                          ),
                          Text(
                            "USERNAME",
                            style: AppStyles.primaryTextStyle().copyWith(
                              fontWeight: FontWeight.bold,
                              fontSize: 20.sp,
                            ),
                          ),
                          SizedBox(
                            height: 8.h,
                          ),
                          InputField(
                            controller: signinBloc.userName,
                            color: AppColors.whiteColor,
                            borderRadius: 20.r,
                          ),
                          SizedBox(
                            height: 32.h,
                          ),
                          Text(
                            "PASSWORD",
                            style: AppStyles.primaryTextStyle().copyWith(
                              fontWeight: FontWeight.bold,
                              fontSize: 20.sp,
                            ),
                          ),
                          SizedBox(
                            height: 8.h,
                          ),
                          InputField(
                            controller: signinBloc.password,
                            color: AppColors.whiteColor,
                            borderRadius: 20.r,
                          ),
                          SizedBox(
                            height: 48.h,
                          ),
                          Center(
                            child: GestureDetector(
                              onTap: () => signinBloc.add(SigningInEvent()),
                              child: Container(
                                width: 400.w,
                                padding: EdgeInsets.symmetric(
                                    horizontal: 16.w, vertical: 16.h),
                                decoration: BoxDecoration(
                                  color: AppColors.blueColor,
                                  borderRadius: BorderRadius.circular(20.r),
                                ),
                                child: Center(
                                  child: Text(
                                    "Login",
                                    style: AppStyles.primaryTextStyle()
                                        .copyWith(color: AppColors.whiteColor),
                                  ),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
