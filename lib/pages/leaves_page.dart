import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hnndes_project/api/leaves_api.dart';
import 'package:hnndes_project/blocs/leaves_bloc/leaves_bloc.dart';
import 'package:hnndes_project/depends/app_colors.dart';
import 'package:hnndes_project/depends/styles.dart';
import 'package:hnndes_project/main.dart';
import 'package:hnndes_project/pages/signin_page.dart';
import 'package:hnndes_project/widgets/loading_widget.dart';

class LeavesPage extends StatelessWidget {
  LeavesPage({super.key});
  final LeavesBloc leavesBloc = LeavesBloc(leavesApi: LeavesApi());
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => leavesBloc..add(LeavesInitialEvent()),
      child: SafeArea(
        child: Scaffold(
          appBar: AppBar(
            title: Text(
              "My Leaves",
              style: AppStyles.primaryTextStyle().copyWith(
                fontSize: 26.sp,
              ),
            ),
            centerTitle: true,
            backgroundColor: AppColors.whiteColor,
            elevation: 5,
          ),
          backgroundColor: AppColors.whiteColor,
          body: BlocConsumer<LeavesBloc, LeavesState>(
            listener: (context, state) {
              if (state is UnAuthorizedState) {
                Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (context) => SigninPage(),
                ));
              }
            },
            builder: (context, state) {
              if (state is LeavesInitial) {
                return const LoadingWidget();
              } else if (state is LeavesFirstPageFailed) {
                return const Center(
                  child: Text("Error while loading"),
                );
              } else {
                return Column(
                  children: [
                    Container(
                      height: 200.h,
                      padding: EdgeInsets.symmetric(
                          horizontal: 32.w, vertical: 16.h),
                      decoration: BoxDecoration(
                        color: Colors.purple[300],
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(200.r)),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            localStorage?.get("emp_name"),
                            style: AppStyles.primaryTextStyle().copyWith(
                              color: AppColors.whiteColor,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          SizedBox(
                            height: 8.h,
                          ),
                          LinearProgressIndicator(
                            value: leavesBloc.userLeaves.toDouble() /
                                leavesBloc.leaveCount.maxAnnual!,
                            backgroundColor: Colors.greenAccent,
                            valueColor: AlwaysStoppedAnimation<Color>(
                                AppColors.redColor),
                          ),
                          SizedBox(
                            height: 12.h,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "${leavesBloc.userLeaves} Leaves",
                                style: AppStyles.primaryTextStyle().copyWith(
                                    fontSize: 16.sp,
                                    color: AppColors.whiteColor),
                              ),
                              Text(
                                "${leavesBloc.leaveCount.maxAnnual} Leaves",
                                style: AppStyles.primaryTextStyle().copyWith(
                                    fontSize: 16.sp,
                                    color: AppColors.whiteColor),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                    Expanded(
                      child: ListView.builder(
                        controller: leavesBloc.sc,
                        itemCount: leavesBloc.leaves.length,
                        itemBuilder: (context, index) => leaveWidget(index),
                      ),
                    ),
                    BlocBuilder<LeavesBloc, LeavesState>(
                      builder: (context, state) {
                        if (state is LeavesLoading) {
                          return const CircularProgressIndicator();
                        } else if (state is LeavesLoaded) {
                          return Container();
                        } else if (state is LeavesError) {
                          return GestureDetector(
                            onTap: () => leavesBloc.add(LeavesFetchingEvent()),
                            child: SizedBox(
                              height: 100.h,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    "Error, click to try again",
                                    style: AppStyles.primaryTextStyle(),
                                  ),
                                  const Icon(Icons.refresh),
                                ],
                              ),
                            ),
                          );
                        }
                        return Container();
                      },
                    )
                  ],
                );
              }
            },
          ),
        ),
      ),
    );
  }

  Container leaveWidget(int index) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 16.w, vertical: 16.h),
      padding: EdgeInsets.symmetric(horizontal: 16.w, vertical: 12.h),
      decoration: BoxDecoration(
          color: AppColors.whiteColor,
          borderRadius: BorderRadius.circular(15.r),
          boxShadow: [
            BoxShadow(
              blurRadius: 5,
              color: AppColors.blackColor.withOpacity(0.5),
              spreadRadius: 2,
            )
          ]),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              SizedBox(
                width: 37.w,
                height: 37.w,
                child: Image.asset("assets/images/date.png"),
              ),
              SizedBox(
                width: 12.w,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Applied Duration",
                    style: AppStyles.primaryTextStyle().copyWith(
                      fontSize: 22.sp,
                      color: AppColors.grey,
                    ),
                  ),
                  SizedBox(
                    height: 8.h,
                  ),
                  Row(
                    children: [
                      Text(
                        leavesBloc.leaves[index].absenceFrom != null
                            ? leavesBloc.leaves[index].absenceFrom!
                                .substring(0, 10)
                            : "",
                        style: AppStyles.primaryTextStyle(),
                      ),
                      Text(
                        "  to  ",
                        style: AppStyles.primaryTextStyle(),
                      ),
                      Text(
                        leavesBloc.leaves[index].absenceTo != null
                            ? leavesBloc.leaves[index].absenceTo!
                                .substring(0, 10)
                            : "",
                        style: AppStyles.primaryTextStyle(),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          ),
          SizedBox(
            height: 16.h,
          ),
          Row(
            children: [
              SizedBox(
                width: 37.w,
                height: 37.w,
                child: Image.asset("assets/images/note.png"),
              ),
              SizedBox(
                width: 12.w,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Notes",
                    style: AppStyles.primaryTextStyle().copyWith(
                      color: AppColors.grey,
                      fontSize: 22.sp,
                    ),
                  ),
                  SizedBox(
                    height: 8.h,
                  ),
                  Text(
                    leavesBloc.leaves[index].notes != null &&
                            leavesBloc.leaves[index].notes!.isNotEmpty
                        ? leavesBloc.leaves[index].notes!
                        : "no notes",
                    style: AppStyles.primaryTextStyle(),
                  ),
                ],
              ),
            ],
          ),
          SizedBox(
            height: 16.h,
          ),
          Row(
            children: [
              SizedBox(
                width: 37.w,
                height: 37.w,
                child: Image.asset("assets/images/status.png"),
              ),
              SizedBox(
                width: 12.w,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Status",
                    style: AppStyles.primaryTextStyle().copyWith(
                      color: AppColors.grey,
                      fontSize: 22.sp,
                    ),
                  ),
                  SizedBox(
                    height: 8.h,
                  ),
                  Text(
                    leavesBloc.leaves[index].statusName ?? "",
                    style: AppStyles.primaryTextStyle(),
                  ),
                ],
              ),
            ],
          )
        ],
      ),
    );
  }
}
