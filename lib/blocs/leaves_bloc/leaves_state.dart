part of 'leaves_bloc.dart';

@immutable
abstract class LeavesState {}

class LeavesInitial extends LeavesState {}

class LeavesFirstPageLoaded extends LeavesState {}

class LeavesFirstPageFailed extends LeavesState {}

class LeavesLoading extends LeavesState {}

class LeavesLoaded extends LeavesState {}

class LeavesError extends LeavesState {}

class LeavesEnded extends LeavesState {}

class UnAuthorizedState extends LeavesState {}
