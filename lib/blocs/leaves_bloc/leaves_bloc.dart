import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hnndes_project/api/leaves_api.dart';
import 'package:hnndes_project/data/leave_count_model.dart';
import 'package:hnndes_project/data/leave_model.dart';
import 'package:meta/meta.dart';

part 'leaves_event.dart';
part 'leaves_state.dart';

class LeavesBloc extends Bloc<LeavesEvent, LeavesState> {
  final LeavesApi leavesApi;
  int pageNumber = 1;
  int totalPages = 0;
  List<LeaveModel> leaves = [];
  LeaveCount leaveCount = LeaveCount();
  int userLeaves = 0;
  ScrollController sc = ScrollController();
  LeavesBloc({required this.leavesApi}) : super(LeavesInitial()) {
    on<LeavesEvent>((event, emit) async {
      if (event is LeavesInitialEvent) {
        emit(LeavesInitial());
        var response = await leavesApi.getAllLeaves(pageNumber);
        var res = await leavesApi.getLeavesCount();
        if (response == "unAuth") {
          emit(UnAuthorizedState());
        } else if (response != "error" && res != "error") {
          emit(LeavesFirstPageLoaded());
          initScroll();
          for (var leave in response["data"]["leaves"]) {
            leaves.add(LeaveModel.fromJson(leave));
          }
          pageNumber++;
          totalPages = response["data"]["totalPages"];
          leaveCount = LeaveCount.fromJson(res["data"]);
          calcLeaves(leaveCount);
        } else {
          emit(LeavesFirstPageFailed());
        }
      } else if (event is LeavesFetchingEvent) {
        emit(LeavesLoading());
        var response = await leavesApi.getAllLeaves(pageNumber);
        if (response != "error") {
          emit(LeavesLoaded());
          for (var leave in response["data"]["leaves"]) {
            leaves.add(LeaveModel.fromJson(leave));
          }
          pageNumber++;
        } else {
          emit(LeavesError());
        }
      }
    });
  }

  initScroll() {
    sc.addListener(() {
      if (sc.position.pixels >= sc.position.maxScrollExtent) {
        if (pageNumber <= totalPages) {
          add(LeavesFetchingEvent());
        }
      }
    });
  }

  calcLeaves(LeaveCount leaveCount) {
    userLeaves = leaveCount.annual! +
        leaveCount.death! +
        leaveCount.marriage! +
        leaveCount.maternity! +
        leaveCount.others! +
        leaveCount.sickness! +
        leaveCount.unpaid! +
        leaveCount.workAccident!;
  }
}
