part of 'signin_bloc.dart';

@immutable
abstract class SigninEvent {}

class InitEvent extends SigninEvent {}

class SigningInEvent extends SigninEvent {}
