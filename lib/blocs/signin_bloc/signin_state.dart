part of 'signin_bloc.dart';

@immutable
abstract class SigninState {}

class SigninInitial extends SigninState {}

class LoadingState extends SigninState {}

class SigninSuccess extends SigninState {}

class SigninFailure extends SigninState {}
