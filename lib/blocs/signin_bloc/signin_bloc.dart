import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hnndes_project/api/signin_api.dart';
import 'package:hnndes_project/main.dart';
import 'package:meta/meta.dart';

part 'signin_event.dart';
part 'signin_state.dart';

class SigninBloc extends Bloc<SigninEvent, SigninState> {
  final SigninApi signinApi;
  TextEditingController userName = TextEditingController();
  TextEditingController password = TextEditingController();
  SigninBloc({required this.signinApi}) : super(SigninInitial()) {
    on<SigninEvent>((event, emit) async {
      if (event is SigningInEvent) {
        emit(LoadingState());
        try {
          var response = await signinApi.signin(userName.text, password.text);
          if (response != "error") {
            emit(SigninSuccess());
            storeDataLocally(response);
          } else {
            emit(SigninFailure());
          }
        } catch (e) {
          emit(SigninFailure());
        }
      }
    });
  }
  storeDataLocally(response) {
    localStorage?.put("access_token", response["data"]["accessToken"]);
    localStorage?.put("emp_id", response["data"]["user"]["employee"]["id"]);
    localStorage?.put(
        "emp_name", response["data"]["user"]["employee"]["fullName"]);
  }
}
