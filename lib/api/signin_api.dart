import 'package:dio/dio.dart';

class SigninApi {
  signin(String userName, String password) async {
    const String url = "https://staging.api.hr-portals.com/api/v1/Auth/login";
    final dio = Dio();
    try {
      var response = await dio
          .post(url, data: {"username": userName, "password": password});
      if (response.statusCode == 200) {
        print(response.data);
        return response.data;
      } else {
        return "error";
      }
    } catch (e) {
      rethrow;
    }
  }
}
