import 'package:dio/dio.dart';
import 'package:hnndes_project/main.dart';

class LeavesApi {
  getAllLeaves(int pageNumber) async {
    const String url = "https://staging.api.hr-portals.com/api/v1/Leave/List";
    final dio = Dio();
    dio.options.headers['companyId'] = "1";
    dio.options.headers['departmentId'] = "1";
    dio.options.headers['employeeId'] = "6";
    dio.options.headers['pageNumber'] = pageNumber.toString();
    dio.options.headers['pageSize'] = "7";
    dio.options.headers["Authorization"] =
        "Bearer ${localStorage?.get("access_token")}";
    try {
      var response = await dio.get(url);
      if (response.statusCode == 200) {
        return response.data;
      } else if (response.statusCode == 401) {
        return "unAuth";
      } else {
        return "error";
      }
    } catch (e) {
      return "error";
    }
  }

  getLeavesCount() async {
    String url =
        "https://staging.api.hr-portals.com/api/v1/Employee/LeaveCount/${localStorage?.get("emp_id").toString()}";
    final dio = Dio();
    dio.options.headers["Authorization"] =
        "Bearer ${localStorage?.get("access_token")}";
    try {
      var response = await dio.get(url);
      if (response.statusCode == 200) {
        return response.data;
      } else if (response.statusCode == 401) {
        return "unAuth";
      } else {
        return "error";
      }
    } catch (e) {
      return "error";
    }
  }
}
